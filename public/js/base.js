$(document).on("click", ".confirm", function (e) {
    e.preventDefault();

    var target = $(this);
    if ("true" == target.attr("confirmed")) {
        return;
    }

    bootbox.confirm(target.attr("rel"), function (result) {
        if (result) {
            target.attr("confirmed", "true")
                  .trigger("click");
        } else {
            target.attr("confirmed", "");
        }
    });
});

$(document).on("click", ".fake-form", function (e) {
    e.preventDefault();

    var target = $(this);
    if (target.hasClass("confirm") && "true" != target.attr("confirmed")) {
        return;
    }

    var formData = {};
    try {
        formData = JSON.parse(target.attr("data-form"));
    } catch (exception) {
        target.attr("confirmed", "");
        return;
    }

    var inputFactory = function (name, value) {
        return "<input type='hidden' name='" + name + "' value='" + value + "' />";
    };

    var formBody = "";
    if ("undefined" != typeof formData.data) {
        for (var k in formData.data) {
            if (! formData.data.hasOwnProperty(k)) {
                continue;
            }

            formBody += inputFactory(k, formData.data[k]);
        }
    }

    formBody += "</form>";
    formBody = "<form action='" + formData.action + "' method='" + ("undefined" != typeof formData.method ? formData.method : "get") + "'>"
        + formBody;

    var form = $(formBody);
    $("body").append(form);
    form.submit();
});

$(document).on("click", ".btn-cancel", function (e) {
    e.preventDefault();

    location.href = e.attr("rel");
});

$(document).on("click", ".fileupload .btn-file", function () {
    var container = $(this).parents(".fileupload");
    if (container.attr("data-old")) {
        return true;
    }

    var img = container.find(".fileupload-preview img");
    if (! img || ! img.attr("src")) {
        return true;
    }

    container.attr("data-old", img.attr("src"));
    return true;
});

$(document).on("click", ".fileupload .fileupload-exists", function () {
    var container = $(this).parents(".fileupload");
    if (container.attr("data-old")) {
        container.find(".fileupload-preview").html("<img src='" + container.attr("data-old") + "' />");
    }
});

$(document).ready(function () {
    // Select
    $("select.select2").select2();

    // Input[type=file]
    var timestamp = new Date().getTime();
    $("input[type=file]").each(function () {
        var input = $(this).clone().wrap("<div />").parent().html();

        $(this).replaceWith($('<div class="fileupload fileupload-new" data-provides="fileupload">'
            + '<div class="fileupload-preview thumbnail" style="width: 200px; height: 150px;">'
                + '<img src="' + $(this).attr("data-rel") + '?time=' + timestamp +  '" />'
            + '</div>'
            + '<div>'
                + '<span class="btn btn-file"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Change</span>'
                    + input
                    + '</span>'
                + '<a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>'
            + '</div>'
        + '</div>'));
    });

    // Navigation
    var target = $(".nav .active").parents("li");
    while (target.length) {
        target.addClass("active");
        target = target.parents("li");
    }
});