<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Zend\ModuleManager\Feature\ServiceProviderInterface;
use Zend\Mvc\Controller\Plugin\FlashMessenger;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\View\Http\ViewManager;
use Zend\ServiceManager\ServiceLocatorInterface;

class Module implements
    ServiceProviderInterface
{
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager        = $e->getApplication()->getEventManager();

        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);

        $eventManager->attach(MvcEvent::EVENT_RENDER, function (MvcEvent $e) {
            /** @var $viewManager ViewManager */
            $viewManager = $e->getApplication()->getServiceManager()->get('ViewManager');
            /** @var $flashMessenger \Zend\View\Helper\FlashMessenger */
            $flashMessenger = $viewManager->getHelperManager()->get('FlashMessenger');

            $flashMessenger->setMessageCloseString('</div>');
            $flashMessenger->setMessageOpenFormat('<div%s><a class="close" data-dismiss="alert">×</a>');
            $flashMessenger->setMessageSeparatorString('</div><div%s><a class="close" data-dismiss="alert">×</a>');
        });
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    /**
     * Expected to return \Zend\ServiceManager\Config object or array to
     * seed such an object.
     *
     * @return array|\Zend\ServiceManager\Config
     */
    public function getServiceConfig()
    {
        return array();
    }
}
