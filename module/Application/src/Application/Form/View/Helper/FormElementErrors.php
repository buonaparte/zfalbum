<?php

namespace Application\Form\View\Helper;

use DluTwBootstrap\Form\View\Helper\FormElementErrorsTwb;
use Zend\Form\ElementInterface;

class FormElementErrors extends FormElementErrorsTwb
{
    public function render(ElementInterface $element, array $attributes = array())
    {
        $this->setMessageOpenFormat('<ul class="errors"><li class="alert alert-error">')
             ->setMessageSeparatorString('</li><li class="alert alert-error">')
             ->setMessageCloseString('</li></ul>');

        return parent::render($element, $attributes);
    }
}