<?php

namespace Album\View\Helper;

use Album\Service\WebPathResolver;
use Zend\View\Helper\AbstractHelper;

class Img extends AbstractHelper
{
    /**
     * @var WebPathResolver
     */
    protected $resolver;

    public function __construct(WebPathResolver $resolver)
    {
        $this->resolver = $resolver;
    }

    public function __invoke($image, $entityOrClass)
    {
        return $this->resolver->getPath($entityOrClass, $image);
    }
}