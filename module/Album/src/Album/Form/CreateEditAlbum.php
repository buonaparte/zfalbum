<?php

namespace Album\Form;

use Album\Entity\Album;
use Album\Form\Fieldset\AlbumFieldset;
use BnpBase\Form\FormEvent;
use BnpBase\Form\ProvideEventsForm;
use Doctrine\Common\Persistence\ObjectManager;
use DoctrineModule\Persistence\ObjectManagerAwareInterface;
use Zend\Form\ElementInterface;
use Zend\InputFilter\InputFilterProviderInterface;

class CreateEditAlbum extends ProvideEventsForm implements
    ObjectManagerAwareInterface
{
    protected  $objectManager;

    public function __construct($name = null)
    {
        parent::__construct($name ?: 'Album');

        $this->add(array(
            'name' => 'submit',
            'type' => 'submit'
        ));

        $this->add(array(
            'name' => 'cancel',
            'type' => 'button'
        ));
    }

    public function init()
    {
        parent::init();

        $this->add(array(
            'name' => 'album',
            'type' => 'Album\Form\AlbumFieldset',
            'options' => array(
                'use_as_base_fieldset' => true
            )
        ));

        /** @var $baseFieldset AlbumFieldset */
        $baseFieldset = $this->getBaseFieldset();

        $baseFieldset->add(array(
            'name' => 'tracks',
            'type' => 'DoctrineModule\Form\Element\ObjectSelect',
            'options' => array(
                'label' => 'Tracks',
                'object_manager' => $this->getObjectManager(),
                'target_class' => 'Album\Entity\Track',
                'find_method' => array(
                    'name' => 'findForForm',
                    'params' => array()
                )
            ),
            'attributes' => array(
                'multiple' => true
            )
        ));

        $events = $this->getEventManager();
        $events->attach(FormEvent::EVENT_BIND, array($this, 'onBindEvent'));
        $events->attachAggregate($baseFieldset);
    }

    public function onBindEvent(FormEvent $event)
    {
        $object = $event->getObject();
        $tracksField = $this->getBaseFieldset()->get('tracks');

        if (! $object instanceof Album || ! $tracksField instanceof ElementInterface) {
            return;
        }

        $tracksField->setOptions(array_merge_recursive(
            $tracksField->getOptions(),
            array(
                'find_method' => array(
                    'params' => array(
                        'album' => $object
                    )
                )
            )
        ));
    }

    /**
     * @param ObjectManager $objectManager
     */
    public function setObjectManager(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    /**
     * Get the object manager
     *
     * @return ObjectManager
     */
    public function getObjectManager()
    {
        return $this->objectManager;
    }
}