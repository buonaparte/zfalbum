<?php

namespace Album\Form;

use BnpBase\Form\ProvideEventsForm;
use Doctrine\Common\Persistence\ObjectManager;
use DoctrineModule\Persistence\ObjectManagerAwareInterface;
use Zend\InputFilter\InputFilterProviderInterface;

class CreateEditTrack extends ProvideEventsForm implements
    ObjectManagerAwareInterface,
    InputFilterProviderInterface
{
    protected $objectManager;

    public function __construct($name = null)
    {
        parent::__construct($name ?: 'Track');

        $this->add(array(
            'name' => 'submit',
            'type' => 'submit'
        ));

        $this->add(array(
            'name' => 'cancel',
            'type' => 'button'
        ));
    }

    public function init()
    {
        parent::init();

        $this->add(array(
            'name' => 'track',
            'type' => 'Album\Form\TrackFieldset',
            'options' => array(
                'use_as_base_fieldset' => true
            )
        ));

        $this->getBaseFieldset()->add(array(
            'name' => 'album',
            'type' => 'DoctrineModule\Form\Element\ObjectSelect',
            'options' => array(
                'label' => 'Album',
                'object_manager' => $this->getObjectManager(),
                'target_class' => 'Album\Entity\Album',
                'empty_option' => 'None'
            )
        ));
    }

    /**
     * Set the object manager
     *
     * @param ObjectManager $objectManager
     */
    public function setObjectManager(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    /**
     * Get the object manager
     *
     * @return ObjectManager
     */
    public function getObjectManager()
    {
        return $this->objectManager;
    }

    /**
     * Should return an array specification compatible with
     * {@link Zend\InputFilter\Factory::createInputFilter()}.
     *
     * @return array
     */
    public function getInputFilterSpecification()
    {
        return array();
    }
}