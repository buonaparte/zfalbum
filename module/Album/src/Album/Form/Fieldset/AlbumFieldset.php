<?php

namespace Album\Form\Fieldset;

use Album\Entity\AlbumInterface;
use Album\Service\WebPathResolver;
use BnpBase\Form\FormEvent;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;

class AlbumFieldset extends Fieldset implements
    InputFilterProviderInterface,
    ListenerAggregateInterface
{
    protected $objectManager;

    /**
     * @var WebPathResolver
     */
    protected $webPathResolver;

    public function __construct($name = null, $options = array())
    {
        parent::__construct($name ?: 'Album', $options);
    }

    public function init()
    {
        $this->add(array(
            'name' => 'id',
            'type' => 'hidden'
        ));

        $this->add(array(
            'name' => 'title',
            'type' => 'text',
            'options' => array(
                'label' => 'Title'
            )
        ));

        $this->add(array(
            'name' => 'artist',
            'type' => 'text',
            'options' => array(
                'label' => 'Artist'
            )
        ));

        $this->add(array(
            'name' => 'image',
            'type' => 'file',
            'options' => array(
                'label' => 'Cover'
            ),
            'attributes' => array(
                'id' => 'image',
                'data-rel' => null
            )
        ));
    }

    /**
     * Should return an array specification compatible with
     * {@link Zend\InputFilter\Factory::createInputFilter()}.
     *
     * @return array
     */
    public function getInputFilterSpecification()
    {
        return array(
            'id' => array(
                'required' => false
            ),
            'title' => array(
                'required' => true,
                'filters' => array(
                    array('name' => 'StringTrim'),
                    array('name' => 'StripTags')
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 2,
                            'max' => 100
                        )
                    )
                )
            ),
            'artist' => array(
                'required' => true,
                'filters' => array(
                    array('name' => 'StringTrim'),
                    array('name' => 'StripTags')
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 2,
                            'max' => 100
                        )
                    )
                )
            ),
            'image' => array(
                'required' => false,
                'filters' => array(
                    array(
                        'name' => 'filerenameupload',
                        'options' => array(
                            'target' => './data/upload',
                            'overwrite' => true,
                            'randomize' => true,
                            'use_upload_name' => true
                        )
                    )
                ),
                'validators' => array(
                    array(
                        'name' => 'filemimetype',
                        'options' => array(
                            'image',
                            'enableHeaderCheck' => true
                        )
                    )
                )
            ),
            'tracks' => array(
                'required' => false
            )
        );
    }

    /**
     * Attach one or more listeners
     *
     * Implementors may add an optional $priority argument; the EventManager
     * implementation will pass this to the aggregate.
     *
     * @param EventManagerInterface $events
     *
     * @return void
     */
    public function attach(EventManagerInterface $events)
    {
        $events->attach(FormEvent::EVENT_POST_BIND, array($this, 'addImagePreview'));
    }

    /**
     * Detach all previously attached listeners
     *
     * @param EventManagerInterface $events
     * @throws \RuntimeException
     * @return void
     */
    public function detach(EventManagerInterface $events)
    {
        throw new \RuntimeException('Not used');
    }

    public function addImagePreview(FormEvent $event)
    {
        /** @var $album AlbumInterface */
        $album = $event->getObject();

        if (! $album->getImage()) {
            return;
        }

        $this->get('image')
             ->setAttribute('data-rel', $this->webPathResolver->getPath($album, $album->getImage()));
    }

    public function setWebPathResolver(WebPathResolver $webPathResolver)
    {
        $this->webPathResolver = $webPathResolver;
    }

    protected function getWebPathResolver()
    {
        if (null === $this->webPathResolver) {
            throw new \RuntimeException('Web Path Resolver service not loaded');
        }

        return $this->webPathResolver;
    }
}