<?php

namespace Album\Form\Fieldset;

use Album\Entity\Track;
use Zend\Form\Fieldset;
use Zend\InputFilter\InputFilterProviderInterface;

class TrackFieldset extends Fieldset implements
    InputFilterProviderInterface
{
    protected $objectManager;

    public function __construct($name = null, $options = array())
    {
        parent::__construct($name ?: 'Track', $options);
    }

    public function init()
    {
        $this->setObject(new Track());

        $this->add(array(
            'name' => 'id',
            'type' => 'hidden'
        ));

        $this->add(array(
            'name' => 'name',
            'type' => 'text',
            'options' => array(
                'label' => 'Name'
            )
        ));

        $this->add(array(
            'name' => 'duration',
            'type' => 'text',
            'options' => array(
                'label' => 'Duration'
            )
        ));
    }

    /**
     * Should return an array specification compatible with
     * {@link Zend\InputFilter\Factory::createInputFilter()}.
     *
     * @return array
     */
    public function getInputFilterSpecification()
    {
        return array(
            'name' => array(
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim')
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 2,
                            'max' => 100
                        )
                    )
                )
            ),
            'duration' => array(
                'required' => false,
                'filters' => array(
                    array('name' => 'Int')
                )
            ),
            'album' => array(
                'required' => false
            )
        );
    }
}