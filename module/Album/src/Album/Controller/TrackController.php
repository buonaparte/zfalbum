<?php

namespace Album\Controller;

use Album\Entity\Track;
use BnpBase\Mapper\BaseMapperInterface;
use BnpBase\Mapper\FilterInterface;
use BnpBase\Service\CrudService;
use Zend\Form\FormInterface;
use Zend\Http\Request;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Paginator\Paginator;
use Zend\View\Model\ViewModel;

class TrackController extends AbstractActionController
{
    /**
     * @var FormInterface
     */
    protected $trackForm;

    /**
     * @var BaseMapperInterface
     */
    protected $trackMapper;

    /**
     * @var CrudService
     */
    protected $trackService;

    public function __construct(BaseMapperInterface $mapper)
    {
        $this->trackMapper = $mapper;
    }

    public function setTrackService(CrudService $service)
    {
        $this->trackService = $service;
    }

    public function getTrackService()
    {
        if (null === $this->trackService) {
            /** @var $service CrudService */
            $service = $this->getServiceLocator()->get('Album\Service\Track');
            $this->setTrackService($service);
        }

        return $this->trackService;
    }

    public function setTrackForm(FormInterface $form)
    {
        $this->trackForm = $form;
    }

    public function getTrackForm()
    {
        if (null === $this->trackForm) {
            /** @var $form FormInterface */
            $form = $this->getServiceLocator()->get('Album\Form\CreateEditTrack');
            $this->setTrackForm($form);
        }

        return $this->trackForm;
    }

    public function indexAction()
    {
        $orderBy = $this->params()->fromRoute('order_by', 'id');
        $order = $this->params()->fromRoute('order', 'desc');

        /** @var $filter FilterInterface */
        $filter = $this->trackMapper->getFilter();
        try {
            $filter->order($orderBy, $order);
        } catch (\Exception $e) {
            $filter->order('id');
        }

        /** @var $tracks Paginator */
        $tracks = $this->trackMapper->findAll($filter, true);

        $tracks->setCurrentPageNumber($page = $this->params()->fromRoute('page', 1));
        $tracks->setItemCountPerPage(10);

        return new ViewModel(array(
            'tracks' => $tracks,
            'orderBy' => $orderBy,
            'order' => $order,
            'page' => $page
        ));
    }

    public function addAction()
    {
        /** @var $request Request */
        $request = $this->getRequest();

        /** @var $form FormInterface */
        $form = $this->getTrackForm();
        $form->get('submit')->setValue('Add');

        /** @var $request Request */
        $request = $this->getRequest();
        $request->getQuery();
        if ($request->isPost()) {
            $form->bind(new Track());
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $track = $this->getTrackService()->create($form, $request->getPost());

                if ($track) {
                    $this->flashMessenger()->addSuccessMessage(sprintf('Track %s saved', $track->getName()));
                    return $this->redirect()->toRoute('album/track');
                }
            }
        }

        return new ViewModel(array(
            'form' => $form
        ));
    }

    public function editAction()
    {
        /** @var $track Track */
        $track = null;
        try {
            $track = $this->trackMapper->findById($id = $this->params()->fromRoute('id', 0));
            if (! $track) {
                throw new \Exception();
            }
        } catch (\Exception $e) {
            return $this->redirect()->toRoute('album');
        }

        /** @var $form FormInterface */
        $form = $this->getTrackForm();
        $form->bind($track);
        $form->get('submit')->setValue('Save');

        /** @var $request Request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $this->getTrackService()->update($form, $request->getPost(), $track);

                if ($track) {
                    $this->flashMessenger()->addSuccessMessage('Changes saved');
                    return $this->redirect()->toRoute('album/track/track', array('action' => 'edit', 'id' => $id));
                }
            }
        }

        return new ViewModel(array(
            'form' => $form,
            'track' => $track
        ));
    }

    public function deleteAction()
    {
        /** @var $request Request */
        $request = $this->getRequest();
        if (! $request->isPost()) {
            return $this->redirect()->toRoute('album/track');
        }

        /** @var $track Track */
        $track = null;
        try {
            $track = $this->trackMapper->findById($id = $this->params()->fromRoute('id', 0));
            if (! $track) {
                throw new \Exception();
            }
        } catch (\Exception $e) {
            return $this->redirect()->toRoute('album/track');
        }

        $this->trackMapper->remove($track);
        $this->flashMessenger()->addInfoMessage(sprintf('Track %s deleted', $track->getName()));

        return $this->redirect()->toRoute('album/track');
    }
}