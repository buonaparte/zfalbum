<?php

namespace Album\Controller;

use Album\Entity\Album;
use Album\Form\AlbumForm;
use Album\Form\CreateEditAlbum;
use BnpBase\Mapper\BaseMapperInterface;
use BnpBase\Mapper\FilterInterface;
use BnpBase\Service\CrudService;
use Zend\Db\Sql\Select;
use Zend\Form\Element;
use Zend\Form\Fieldset;
use Zend\Form\Form;
use Zend\Form\FormInterface;
use Zend\Http\Request;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Paginator\Paginator;
use Zend\Stdlib\ArrayUtils;
use Zend\Stdlib\ResponseInterface;
use Zend\View\Model\ViewModel;

class AlbumController extends AbstractActionController
{
    /**
     * @var BaseMapperInterface
     */
    protected $albumMapper;

    /**
     * @var FormInterface
     */
    protected $albumForm;

    /**
     * @var CrudService
     */
    protected $albumService;

    public function __construct(BaseMapperInterface $albumMapper)
    {
        $this->albumMapper = $albumMapper;
    }

    public function setAlbumForm(FormInterface $albumForm)
    {
        $this->albumForm = $albumForm;
        return $this;
    }

    /**
     * @return FormInterface
     */
    protected function getAlbumForm()
    {
        if (null === $this->albumForm) {
            /** @var $albumForm FormInterface */
            $albumForm = $this->getServiceLocator()->get('Album\Form\CreateEditAlbum');
            $this->setAlbumForm($albumForm);
        }

        return $this->albumForm;
    }

    public function setAlbumService(CrudService $albumService)
    {
        $this->albumService = $albumService;
        return $this;
    }

    /**
     * @return CrudService
     *
     */
    protected function getAlbumService()
    {
        if (null === $this->albumService) {
            /** @var $albumService CrudService */
            $albumService = $this->getServiceLocator()->get('Album\Service\Album');
            $this->setAlbumService($albumService);
        }

        return $this->albumService;
    }

    public function indexAction()
    {
        $orderBy = $this->params()->fromRoute('order_by', 'id');
        $order = $this->params()->fromRoute('order', 'desc');

        /** @var $filter FilterInterface */
        $filter = $this->albumMapper->getFilter();
        try {
            $filter->order($orderBy, $order);
        } catch (\Exception $e) {
            $filter->order('id');
        }

        /** @var $albums Paginator */
        $albums = $this->albumMapper->findAll($filter, true);

        $albums->setCurrentPageNumber($page = $this->params()->fromRoute('page', 1));
        $albums->setItemCountPerPage(10);

        return new ViewModel(array(
            'albums' => $albums,
            'orderBy' => $orderBy,
            'order' => $order,
            'page' => $page
        ));
    }

    public function addAction()
    {
        /** @var $request Request */
        $request = $this->getRequest();

        /** @var $form FormInterface */
        $form = $this->getAlbumForm();
        $form->get('submit')->setValue('Add');

        /** @var $request Request */
        $request = $this->getRequest();
        $request->getQuery();
        if ($request->isPost()) {
            $form->bind(new Album());
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $album = $this->getAlbumService()->create($form, $request->getPost());

                if ($album) {
                    $this->flashMessenger()->addSuccessMessage(sprintf('Album %s saved', $album->getTitle()));
                    return $this->redirect()->toRoute('album');
                }
            }
        }

        return new ViewModel(array(
            'form' => $form
        ));
    }

    public function editAction()
    {
       /** @var $album Album */
       $album = null;
       try {
           $album = $this->albumMapper->findById($id = $this->params()->fromRoute('id', 0));
           if (! $album) {
               throw new \Exception();
           }
       } catch (\Exception $e) {
           return $this->redirect()->toRoute('album');
       }

        /** @var $form Form */
        $form = $this->getAlbumForm();
        $form->bind($album);
        $form->get('submit')->setValue('Save');

        $prg = $this->fileprg($form);
        if ($prg instanceof ResponseInterface) {
            return $prg;
        }

        /** @var $request Request */
        $request = $this->getRequest();
        if (is_array($prg)) {
            if ($form->isValid()) {
                $this->albumMapper->update($album);

                if ($album) {
                    $this->flashMessenger()->addSuccessMessage('Changes saved');
                    return $this->redirect()->toRoute('album/album', array('action' => 'edit', 'id' => $id));
                }
            } else {
                /** @var $albumFieldset Fieldset */
                $albumFieldset = $form->get('album');
                /** @var $imageElement Element */
                $imageElement = $albumFieldset->get('image');
                if (! $imageElement->getMessages()) {
                    $album->setImage($imageElement->getValue());
                    $this->albumMapper->update($album);
                }
            }
        }

        return new ViewModel(array(
            'form' => $form,
            'album' => $album
        ));
    }

    public function deleteAction()
    {
        /** @var $request Request */
        $request = $this->getRequest();
        if (! $request->isPost()) {
            return $this->redirect()->toRoute('album');
        }

        /** @var $album Album */
        $album = null;
        try {
            $album = $this->albumMapper->findById($id = $this->params()->fromRoute('id', 0));
            if (! $album) {
                throw new \Exception();
            }
        } catch (\Exception $e) {
            return $this->redirect()->toRoute('album');
        }

       $this->albumMapper->remove($album);
       $this->flashMessenger()->addInfoMessage(sprintf('Album %s deleted', $album->getTitle()));

       return $this->redirect()->toRoute('album');
    }
}