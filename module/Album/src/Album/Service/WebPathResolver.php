<?php

namespace Album\Service;

use Zend\View\Helper\BasePath;

class WebPathResolver
{
    /**
     * @var BasePath
     */
    protected $basePath;

    /**
     * @var array|\Traversable
     */
    protected $relativePaths;

    public function __construct(BasePath $basePath, $relativePaths = array())
    {
        if (! is_array($relativePaths) && ! $relativePaths instanceof \Traversable) {
            throw new \InvalidArgumentException('Array | Traversable instance expected');
        }

        $this->relativePaths = array();
        foreach ($relativePaths as $entityName => $relativePath) {
            $this->relativePaths[$entityName] = $relativePath;
        }

        $this->basePath = $basePath;
    }

    /**
     * @param object|string $entity
     * @param string $path
     *
     * @throws \InvalidArgumentException
     * @return string
     */
    public function getPath($entity, $path)
    {
        if (is_object($entity)) {
            $entity = get_class($entity);
        }

        if (! is_string($entity)) {
            throw new \InvalidArgumentException('Entity name expected');
        }

        $basePath = $this->basePath;
        foreach ($this->relativePaths as $entityName => $relativePath) {
            if ($entityName === $entity) {
                return $basePath('/' . trim($relativePath, '/\\') . '/' . ltrim($path, '/\\'));
            }
        }

        return $basePath('/' . ltrim($path, '/\\'));
    }
}