<?php

namespace Album\EventManager;

use Album\Entity\AlbumInterface;
use BnpBase\Mapper\BaseMapper;
use Zend\EventManager\EventInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;

class AlbumMapperListenerAggregate implements ListenerAggregateInterface
{
    protected $publicPath;
    protected $tmpPath;

    public function __construct($tmpPath, $publicPath)
    {
        $this->tmpPath = rtrim($tmpPath, '/\\') . '/';
        $this->publicPath = rtrim($publicPath, '/\\') . '/';
    }

    /**
     * Attach one or more listeners
     *
     * Implementors may add an optional $priority argument; the EventManager
     * implementation will pass this to the aggregate.
     *
     * @param EventManagerInterface $events
     *
     * @return void
     */
    public function attach(EventManagerInterface $events)
    {
        $events->attach(BaseMapper::EVENT_UPDATE, array($this, 'onChange'));
        $events->attach(BaseMapper::EVENT_UPDATE, array($this, 'onChange'));
        $events->attach(BaseMapper::EVENT_REMOVE, array($this, 'onRemove'));
    }

    /**
     * Detach all previously attached listeners
     *
     * @param EventManagerInterface $events
     * @throws \RuntimeException
     * @return void
     */
    public function detach(EventManagerInterface $events)
    {
        throw new \RuntimeException('Not used');
    }

    public function onChange(EventInterface $event)
    {
        /** @var $album AlbumInterface */
        $album = $event->getParam('entity');
        if (! $album) {
            return;
        }

        $albumImage = $album->getImage();
        if (! is_array($albumImage) || ! array_key_exists('tmp_name', $albumImage)) {
            return;
        }
        $albumImage = $albumImage['tmp_name'];

        if (file_exists($source = $albumImage)) {
            $newName = $this->generateFilename($albumImage, $album);
            if (rename($source, $destination = $this->publicPath . $newName) && file_exists($destination)) {
                unlink($source);
                $album->setImage($newName);
            }
        }
    }

    protected function generateFilename($oldName, AlbumInterface $album)
    {
        return preg_replace_callback('#(.*)\.([a-zA-Z0-9]+)$#', function (array $matches) use ($album) {
            return hash('sha1', $album->getId() . $album->getTitle() . $album->getArtist()) . ".{$matches[2]}";
        }, $oldName);
    }

    public function onRemove(EventInterface $event)
    {
        /** @var $album AlbumInterface */
        $album = $event->getParam('entity');

        if (file_exists($filename = $this->publicPath . $album->getImage())) {
            unlink($filename);
        }
    }
}