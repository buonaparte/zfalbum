<?php

namespace Album;

use Album\Controller\AlbumController;
use Album\Controller\TrackController;
use Album\Entity\Album;
use Album\Entity\Track;
use Album\EventManager\AlbumMapperListenerAggregate;
use Album\Form\CreateEditAlbum;
use Album\Form\CreateEditTrack;
use Album\Form\Fieldset\AlbumFieldset;
use Album\Form\Fieldset\TrackFieldset;
use Album\Service\WebPathResolver;
use Album\View\Helper\Img;
use BnpBase\Form\FormEvent;
use BnpBase\Mapper\Adapter\DoctrineAdapter;
use BnpBase\Mapper\BaseMapper;
use BnpBase\Mapper\BaseMapperInterface;
use BnpBase\Service\CrudService;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use DoctrineModule\Persistence\ObjectManagerAwareInterface;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\Form\FormInterface;
use Zend\ModuleManager\Feature\ViewHelperProviderInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Form\FormElementManager;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\ControllerProviderInterface;
use Zend\ModuleManager\Feature\FormElementProviderInterface;
use Zend\Mvc\Controller\ControllerManager;
use Zend\Stdlib\Hydrator\HydratorInterface;
use Zend\View\Helper\BasePath;
use Zend\View\HelperPluginManager;

class Module implements
    ConfigProviderInterface,
    AutoloaderProviderInterface,
    ControllerProviderInterface,
    FormElementProviderInterface,
    ViewHelperProviderInterface
{
    /**
     * Return an array for passing to Zend\Loader\AutoloaderFactory.
     *
     * @return array
     */
    public function getAutoloaderConfig()
    {
        return array(
//            'Zend\Loader\ClassMapAutoloader' => array(
//                __DIR__.'/autoload_classmap.php'
//            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__
                )
            )
        );
    }

    /**
     * Returns configuration to merge with application configuration
     *
     * @return array|\Traversable
     */
    public function getConfig()
    {
        return include __DIR__.'/../../config/module.config.php';
    }

    /**
     * Expected to return \Zend\ServiceManager\Config object or array to seed
     * such an object.
     *
     * @return array|\Zend\ServiceManager\Config
     */
    public function getControllerConfig()
    {
        return array(
            'factories' => array(
                'Album\Controller\Album' => function (ControllerManager $sm) {
                    /** @var $albumMapper BaseMapperInterface */
                    $albumMapper = $sm->getServiceLocator()->get('Album\Mapper\Album');
                    /** @var $albumService CrudService */
                    $albumService = $sm->getServiceLocator()->get('Album\Service\Album');
                    /** @var $formManager FormElementManager */
                    $formManager = $sm->getServiceLocator()->get('FormElementManager');
                    /** @var $albumForm CreateEditAlbum */
                    $albumForm = $formManager->get('Album\Form\CreateEditAlbum');


                    $controller = new AlbumController($albumMapper);
                    $controller->setAlbumService($albumService);
                    $controller->setAlbumForm($albumForm);
                    return $controller;
                },
                'Album\Controller\Track' => function (ControllerManager $sm) {
                    /** @var $trackMapper BaseMapperInterface */
                    $trackMapper = $sm->getServiceLocator()->get('Album\Mapper\Track');
                    /** @var $trackService CrudService */
                    $trackService = $sm->getServiceLocator()->get('Album\Service\Track');
                    /** @var $formManager FormElementManager */
                    $formManager = $sm->getServiceLocator()->get('FormElementManager');
                    /** @var $trackForm FormInterface */
                    $trackForm = $formManager->get('Album\Form\CreateEditTrack');

                    $controller = new TrackController($trackMapper);
                    $controller->setTrackForm($trackForm);
                    $controller->setTrackService($trackService);
                    return $controller;
                }
            )
        );
    }

    /**
     * Expected to return \Zend\ServiceManager\Config object or array to
     * seed such an object.
     *
     * @return array|\Zend\ServiceManager\Config
     */
    public function getServiceConfig()
    {
        return array(
            'invokables' => array(
                'Imagine' => 'Imagine\Gd\Imagine'
            ),
            'factories' => array(
                'Album\Form\AlbumHydrator' => function (ServiceLocatorInterface $sm) {
                    /** @var $objectManager ObjectManager */
                    $objectManager = $sm->get('doctrine.entitymanager.orm_default');

                    return new DoctrineObject($objectManager, 'Album\Entity\Album');
                },
                'Album\Mapper\Album' => function (ServiceLocatorInterface $sm) {
                    /** @var $em EntityManager  */
                    $em = $sm->get('doctrine.entitymanager.orm_default');
                    /** @var $mapperSubscriber ListenerAggregateInterface */
                    $mapperSubscriber = $sm->get('Album\EventManager\AlbumMapperListenerAggregate');

                    $mapper = new BaseMapper(new DoctrineAdapter($em, 'Album\Entity\Album'));
                    $mapper->setEntityPrototype(new Album());
                    $mapper->getEventManager()->attachAggregate($mapperSubscriber);

                    return $mapper;
                },
                'Album\Service\Album' => function (ServiceLocatorInterface $sm) {
                    /** @var $albumMapper BaseMapperInterface */
                    $albumMapper = $sm->get('Album\Mapper\Album');

                    $service = new CrudService($albumMapper);
                    return $service;
                },
                'Album\Form\TrackHydrator' => function (ServiceLocatorInterface $sm) {
                    /** @var $objectManager ObjectManager */
                    $objectManager = $sm->get('doctrine.entitymanager.orm_default');

                    return new DoctrineObject($objectManager, 'Album\Entity\Track');
                },
                'Album\Mapper\Track' => function (ServiceLocatorInterface $sm) {
                    /** @var $entityManager EntityManager */
                    $entityManager = $sm->get('doctrine.entitymanager.orm_default');

                    $mapper = new BaseMapper(new DoctrineAdapter($entityManager, 'Album\Entity\Track'));
                    $mapper->setEntityPrototype(new Track());
                    return $mapper;
                },
                'Album\Service\Track' => function (ServiceLocatorInterface $sm) {
                    /** @var $mapper BaseMapper */
                    $mapper = $sm->get('Album\Mapper\Track');

                    return new CrudService($mapper);
                },
                'Album\EventManager\AlbumMapperListenerAggregate' => function (ServiceLocatorInterface $sm) {
                    return new AlbumMapperListenerAggregate(
                        __DIR__.'/../../../../data/cache',
                        __DIR__.'/../../../../public/uploads/albums'
                    );
                },
                'Album\Service\WebPathResolver' => function (ServiceLocatorInterface $sm) {
                    /** @var $viewHelperManager HelperPluginManager */
                    $viewHelperManager = $sm->get('ViewHelperManager');
                    /** @var $basePath BasePath */
                    $basePath = $viewHelperManager->get('basePath');

                    $relativePaths = array(
                        'Album\Entity\Album' => '/uploads/albums'
                    );

                    return new WebPathResolver($basePath, $relativePaths);
                },
                'Album\TwigLoader' => function (ServiceLocatorInterface $sm) {
                    return new \Twig_Loader_Filesystem(__DIR__.'/../../view');
                }
            ),
            'initializers' => array(
                function ($instance, ServiceLocatorInterface $serviceLocator) {
                    if ($instance instanceof ObjectManagerAwareInterface) {
                        /** @var $objectManager ObjectManager */
                        $objectManager = $serviceLocator->get('doctrine.entitymanager.orm_default');
                        $instance->setObjectManager($objectManager);
                    }
                }
            )
        );
    }

    /**
     * Expected to return \Zend\ServiceManager\Config object or array to
     * seed such an object.
     *
     * @return array|\Zend\ServiceManager\Config
     */
    public function getFormElementConfig()
    {
        return array(
            'factories' => array(
                'Album\Form\CreateEditAlbum' =>  function (FormElementManager $fm) {
                    /** @var $sl ServiceLocatorInterface */
                    $sl = $fm->getServiceLocator();
                    /** @var $hydrator HydratorInterface */
                    $hydrator = $sl->get('Album\Form\AlbumHydrator');
                    /** @var $objectManager ObjectManager */
                    $objectManager = $sl->get('doctrine.entitymanager.orm_default');

                    $form = new CreateEditAlbum();
                    $form->setHydrator($hydrator);
                    $form->setObjectManager($objectManager);

                    return $form;
                },
                'Album\Form\AlbumFieldset' => function (FormElementManager $fm) {
                    /** @var $sl ServiceLocatorInterface */
                    $sl = $fm->getServiceLocator();
                    /** @var $hydrator HydratorInterface */
                    $hydrator = $sl->get('Album\Form\AlbumHydrator');
                    /** @var $webPathResolver WebPathResolver */
                    $webPathResolver = $sl->get('Album\Service\WebPathResolver');


                    $fieldset = new AlbumFieldset();
                    $fieldset->setHydrator($hydrator);
                    $fieldset->setWebPathResolver($webPathResolver);

                    return $fieldset;
                },
                'Album\Form\CreateEditTrack' => function (FormElementManager $fm) {
                    /** @var $sl ServiceLocatorInterface */
                    $sl = $fm->getServiceLocator();
                    /** @var $hydrator HydratorInterface */
                    $hydrator = $sl->get('Album\Form\TrackHydrator');
                    /** @var $objectManager ObjectManager */
                    $objectManager = $sl->get('doctrine.entitymanager.orm_default');

                    $form = new CreateEditTrack();
                    $form->setHydrator($hydrator);
                    $form->setObjectManager($objectManager);

                    return $form;
                },
                'Album\Form\TrackFieldset' => function (FormElementManager $fm) {
                    $sl = $fm->getServiceLocator();
                    /** @var $hydrator HydratorInterface */
                    $hydrator = $sl->get('Album\Form\TrackHydrator');

                    $fieldset = new TrackFieldset();
                    $fieldset->setHydrator($hydrator);

                    return $fieldset;
                }
            )
        );
    }

    /**
     * Expected to return \Zend\ServiceManager\Config object or array to
     * seed such an object.
     *
     * @return array|\Zend\ServiceManager\Config
     */
    public function getViewHelperConfig()
    {
        return array(
            'factories' => array(
                'img' => function (HelperPluginManager $vm) {
                    /** @var $resolver WebPathResolver */
                    $resolver = $vm->getServiceLocator()->get('Album\Service\WebPathResolver');

                    return new Img($resolver);
                }
            )
        );
    }
}