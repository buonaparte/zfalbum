<?php

namespace Album\Entity;

interface TrackInterface
{
    /**
     * @param $id int
     * @return TrackInterface
     */
    public function setId($id);

    /**
     * @return int
     */
    public function getId();

    /**
     * @param $name string
     * @return TrackInterface
     */
    public function setName($name);

    /**
     * @return string
     */
    public function getName();

    /**
     * @param $duration float
     * @return TrackInterface
     */
    public function setDuration($duration);

    /**
     * @return float
     */
    public function getDuration();

    /**
     * @param AlbumInterface $album
     * @return TrackInterface
     */
    public function setAlbum(AlbumInterface $album = null);

    /**
     * @return AlbumInterface
     */
    public function getAlbum();
}