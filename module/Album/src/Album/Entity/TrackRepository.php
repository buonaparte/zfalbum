<?php

namespace Album\Entity;

use Doctrine\ORM\EntityRepository;

class TrackRepository extends EntityRepository
{
    public function findForForm(AlbumInterface $album = null)
    {
        $qb = $this->createQueryBuilder('t');

        $xp = $qb->expr()->isNull('t.album');
        if (null !== $album && $album->getId()) {
            $xp = $qb->expr()->orX(
                $xp,
                $qb->expr()->eq('t.album', $album->getId())
            );
        }

        return $qb->add('where', $xp)
                  ->getQuery()
                  ->execute();
    }
}