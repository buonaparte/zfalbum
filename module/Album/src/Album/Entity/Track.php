<?php

namespace Album\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Track
 * @package Album\Entity
 *
 * @ORM\Entity(repositoryClass="Album\Entity\TrackRepository")
 * @ORM\Table(name="tracks")
 */
class Track implements TrackInterface
{
    /**
     * @var
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var
     *
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @var
     *
     * @ORM\Column(type="float")
     */
    protected $duration;

    /**
     * @var
     *
     * @ORM\ManyToOne(targetEntity="Album", inversedBy="tracks", fetch="EAGER")
     * @ORM\JoinColumn(name="album_id", referencedColumnName="id", nullable=true)
     */
    protected $album;

    /**
     * @param $id int
     * @return TrackInterface
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $name string
     * @return TrackInterface
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $duration float
     * @return TrackInterface
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;
        return $this;
    }

    /**
     * @return float
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * @param AlbumInterface $album
     * @return TrackInterface
     */
    public function setAlbum(AlbumInterface $album = null)
    {
        $this->album = $album;
        return $this;
    }

    /**
     * @return AlbumInterface
     */
    public function getAlbum()
    {
        return $this->album;
    }

    public function __toString()
    {
        $duration = (int) $this->getDuration();
        return sprintf('%s ( %02d:%02d )', $this->getName(), $duration / 60, (int) $duration % 60);
    }
}