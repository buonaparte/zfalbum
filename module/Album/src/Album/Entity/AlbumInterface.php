<?php

namespace Album\Entity;

interface AlbumInterface
{
    /**
     * @param int $id
     * @return AlbumInterface
     */
    public function setId($id);

    /**
     * @return int
     */
    public function getId();

    /**
     * @param string $title
     * @return AlbumInterface
     */
    public function setTitle($title);

    /**
     * @return string
     */
    public function getTitle();

    /**
     * @param string $artist
     * @return AlbumInterface
     */
    public function setArtist($artist);

    /**
     * @return string
     */
    public function getArtist();

    /**
     * @param string $image
     * @return AlbumInterface
     */
    public function setImage($image);

    /**
     * @return string
     */
    public function getImage();

    /**
     * @param \Traversable $tracks
     * @internal param \Album\Entity\TrackInterface $track
     * @return AlbumInterface
     */
    public function addTracks(\Traversable $tracks);

    /**
     * @param \Traversable $tracks
     * @internal param \Album\Entity\TrackInterface|\Traversable $track
     * @return AlbumInterface
     */
    public function removeTracks(\Traversable $tracks);

    /**
     * @return \Traversable
     */
    public function getTracks();
}