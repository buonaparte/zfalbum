<?php

namespace Album\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Album
 * @package Album\Entity
 *
 * @ORM\Entity
 * @ORM\Table(name="albums")
 */
class Album implements AlbumInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $title;

    /**
     * @ORM\Column(type="string")
     */
    protected $artist;

    /**
     * @ORM\Column(type="string")
     */
    protected $image;

    /**
     * @var
     *
     * @ORM\OneToMany(targetEntity="Track", mappedBy="album", cascade={"remove", "detach"})
     */
    protected $tracks;

    public function __construct()
    {
        $this->tracks = new ArrayCollection();
    }

    /**
     * @param $id int
     * @return AlbumInterface
     */
    public function setId($id)
    {
        $this->id = (int) $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $title string
     * @return AlbumInterface
     */
    public function setTitle($title)
    {
        $this->title = (string) $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param $artist string
     * @return AlbumInterface
     */
    public function setArtist($artist)
    {
        $this->artist = (string) $artist;
        return $this;
    }

    /**
     * @return string
     */
    public function getArtist()
    {
        return $this->artist;
    }

    /**
     * @param \Traversable $tracks
     * @internal param \Album\Entity\TrackInterface $track
     * @return AlbumInterface
     */
    public function addTracks(\Traversable $tracks)
    {
        foreach ($tracks as $track) {
            /** @var $track TrackInterface */
            $track->setAlbum($this);
            $this->tracks->add($track);
        }

        return $this;
    }

    /**
     * @param \Album\Entity\TrackInterface|\Traversable $tracks
     * @return AlbumInterface
     */
    public function removeTracks(\Traversable $tracks)
    {
        foreach ($tracks as $track) {
            /** @var $track TrackInterface */
            $track->setAlbum(null);
            $this->tracks->removeElement($track);
        }

        return $this;
    }

    /**
     * @return \Traversable
     */
    public function getTracks()
    {
        return $this->tracks;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return sprintf('%s (by %s)', $this->getTitle(), $this->getArtist());
    }

    /**
     * @param string $image
     * @return AlbumInterface
     */
    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }
}