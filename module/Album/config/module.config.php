<?php

namespace Album;

return array(
    'router' => array(
        'routes' => array(
            'album' => array(
                'type' => 'literal',
                'options' => array(
                    'route' => '/album',
                    'defaults' => array(
                        'controller' => 'Album\Controller\Album',
                        'action' => 'index',
                        'page' => '1',
                        'order_by' => 'id',
                        'order' => 'asc'
                    )
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'list' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '/page[/:page][/order/:order_by[-:order]]',
                            'constraints' => array(
                                'page' => '[1-9][0-9]*',
                                'order_by' => '[a-zA-Z_]+',
                                'order' => 'asc|desc'
                            )
                        )
                    ),
                    'add' => array(
                        'type' => 'literal',
                        'options' => array(
                            'route' => '/add',
                            'defaults' => array(
                                'action' => 'add'
                            )
                        )
                    ),
                    'album' => array(
                        'type' => 'segment',
                        'options' => array(
                            'route' => '[/:action][/:id]',
                            'constraints' => array(
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '[0-9]+'
                            ),
                            'defaults' => array(
                                'controller' => 'Album\Controller\Album',
                                'action' => 'index'
                            )
                        )
                    ),
                    'track' => array(
                        'type' => 'literal',
                        'options' => array(
                            'route' => '/track',
                            'defaults' => array(
                                'controller' => 'Album\Controller\Track',
                                'action' => 'index',
                                'page' => '1',
                                'order_by' => 'id',
                                'order' => 'asc'
                            )
                        ),
                        'may_terminate' => true,
                        'child_routes' => array(
                            'list' => array(
                                'type' => 'segment',
                                'options' => array(
                                    'route' => '/page[/:page][/order/:order_by[-:order]]',
                                    'constraints' => array(
                                        'page' => '[1-9][0-9]*',
                                        'order_by' => '[a-zA-Z_]+',
                                        'order' => 'asc|desc'
                                    )
                                )
                            ),
                            'add' => array(
                                'type' => 'segment',
                                'options' => array(
                                    'route' => '/add',
                                    'defaults' => array(
                                        'action' => 'add'
                                    )
                                )
                            ),
                            'track' => array(
                                'type' => 'segment',
                                'options' => array(
                                    'route' => '[/:action][/:id]',
                                    'constraints' => array(
                                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                        'id' => '[0-9]+'
                                    ),
                                    'defaults' => array(
                                        'controller' => 'Album\Controller\Track',
                                        'action' => 'index'
                                    )
                                )
                            )
                        )
                    )
                )
            )
        )
    ),

    'view_manager' => array(
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
        'template_map' => array(
            'paginator-slide' => __DIR__ . '/../view/layout/slidePaginator.phtml'
        )
    ),

    'doctrine' => array(
        'driver' => array(
            __NAMESPACE__ . '_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(
                    __DIR__ . '/../src/' . __NAMESPACE__ . '/Entity'
                )
            ),
            'orm_default' => array(
                'drivers' => array(
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                )
            )
        )
    ),

    'navigation' => array(
        'default' => array(
            'album' => array(
                'label' => 'Album',
                'route' => 'album',
                'permission' => 'member',
                'pages' => array(
                    'list' => array(
                        'label' => 'List',
                        'route' => 'album'
                    ),
                    'add' => array(
                        'label' => 'Add',
                        'route' => 'album/album',
                        'action' => 'add'
                    )
                )
            ),
            'track' => array(
                'label' => 'Track',
                'route' => 'album/track',
                'permission' => 'member',
                'pages' => array(
                    'list' => array(
                        'label' => 'List',
                        'route' => 'album/track'
                    ),
                    'add' => array(
                        'label' => 'Add',
                        'route' => 'album/track/track',
                        'action' => 'add'
                    )
                )
            )
        )
    )
);