<?php

namespace User\Service;

use Zend\Crypt\Password\Bcrypt;

class PasswordManager
{
    /**
     * @var Bcrypt
     */
    protected $crypt;

    public function __construct(Bcrypt $crypt)
    {
        $this->crypt = $crypt;
    }

    public function createPasswordHash($plain)
    {
        return $this->crypt->create($plain);
    }

    public function checkPassword($plain, $hash)
    {
        return $this->crypt->verify($plain, $hash);
    }
}