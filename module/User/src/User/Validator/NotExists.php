<?php

namespace User\Validator;

use BnpBase\Mapper\BaseMapperInterface;
use BnpBase\Mapper\FilterInterface;
use Zend\Validator\AbstractValidator;
use Zend\Validator\Exception;

class NotExists extends AbstractValidator
{
    const EXISTS = 'exists';

    protected $messageTemplates = array(
        self::EXISTS => "A record already exists with %field% '%value%'"
    );

    protected $messageVariables = array(
        'field' => 'field'
    );

    /**
     * @var BaseMapperInterface
     */
    protected $mapper;

    /**
     * @var string
     */
    protected $field;

    public function __construct(BaseMapperInterface $mapper, $field)
    {
        $this->mapper = $mapper;
        $this->field = $field;
    }

    /**
     * Returns true if and only if $value meets the validation requirements
     *
     * If $value fails validation, then this method returns false, and
     * getMessages() will return an array of messages that explain why the
     * validation failed.
     *
     * @param  mixed $value
     * @return bool
     * @throws Exception\RuntimeException If validation of $value is impossible
     */
    public function isValid($value)
    {
        /** @var $filter FilterInterface */
        $filter = $this->mapper->getFilter();

        if ($this->mapper->findOne($filter->add(array($this->field, $value)))) {
            $this->error(self::EXISTS);
            return false;
        }

        return true;
    }
}