<?php

namespace User\Entity;

interface UserInterface
{
    /**
     * @param int $id
     * @return UserInterface
     */
    public function setId($id);

    /**
     * @return int
     */
    public function getId();

    /**
     * @param string $username
     * @return UserInterface
     */
    public function setUsername($username);

    /**
     * @return string
     */
    public function getUsername();

    /**
     * @param string $password
     * @return UserInterface
     */
    public function setPassword($password);

    /**
     * @return string
     */
    public function getPassword();

    /**
     * @param $level
     * @return UserInterface
     */
    public function setLevel($level);

    /**
     * @return int
     */
    public function getLevel();

    /**
     * @param UserInterface $user
     * @return UserInterface
     */
    public function setParent(UserInterface $user = null);

    /**
     * @return UserInterface|null
     */
    public function getParent();

    /**
     * @return \Traversable
     * @internal UserInterface
     */
    public function getUsers();

    /**
     * @param \Traversable $users
     * @internal UserInterface
     * @return UserInterface
     */
    public function addUsers(\Traversable $users);

    /**
     * @param \Traversable $users
     * @internal UserInterface
     * @return UserInterface
     */
    public function removeUsers(\Traversable $users);
}