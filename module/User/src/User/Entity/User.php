<?php

namespace User\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use ZfcRbac\Identity\IdentityInterface;

/**
 * Class User
 * @package User\Entity
 *
 * @ORM\Entity
 * @ORM\Table(name="users")
 */
class User implements
    UserInterface,
    IdentityInterface
{
    const LEVEL_ADMIN = 1;
    const LEVEL_USER = 2;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $username;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $password;

    /**
     * @var
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="users", fetch="EAGER")
     * @ORM\JoinColumn(name="added_by", referencedColumnName="id", nullable=true)
     */
    protected $parent;

    /**
     * @ORM\OneToMany(targetEntity="User", mappedBy="parent", cascade={"remove"})
     */
    protected $users;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    protected $level;

    /**
     * @var \ReflectionClass
     */
    protected static $reflectionCache;

    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->level = static::LEVEL_USER;
    }

    /**
     * @param int $id
     * @return UserInterface
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $username
     * @return UserInterface
     */
    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $password
     * @return UserInterface
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return array
     */
    public function getRoles()
    {
        switch ($this->getLevel()) {
            case static::LEVEL_ADMIN:
                return array('admin');

            case static::LEVEL_USER:
                return array('member');
        }

        return array();
    }

    /**
     * @param UserInterface $parent
     * @return User
     * @throws \InvalidArgumentException
     */
    public function setParent(UserInterface $parent = null)
    {
        if (static::LEVEL_ADMIN != $parent->getLevel()) {
            throw new \InvalidArgumentException(sprintf(
                'User %s cant be the parent, as it has insufficient permissions', $parent->getUsername()));
        }

        $this->parent = $parent;
        return $this;
    }

    /**
     * @return User|null
     */
    public function getParent()
    {
        return $this->parent;
    }

    public function addUsers(\Traversable $users)
    {
        if (static::LEVEL_ADMIN != $this->getLevel()) {
            throw new \RuntimeException('Not enough permissions');
        }

        foreach ($users as $user) {
            /** @var $user User */
            $user->setParent(null);
            $this->users->add($user);
        }

        return $this;
    }

    /**
     * @param \Traversable $users
     * @return User
     * @throws \RuntimeException
     */
    public function removeUsers(\Traversable $users)
    {
        if (static::LEVEL_ADMIN != $this->getLevel()) {
            throw new \RuntimeException('Not enough permissions');
        }

        foreach ($users as $user) {
            /** @var $user User */
            $user->setParent(null);
            $this->users->removeElement($user);
        }

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @param int $level
     * @return User
     * @throws \InvalidArgumentException
     */
    public function setLevel($level)
    {
        $exists = false;
        foreach ($this->getReflection()->getConstants() as $name => $value) {
            if ('LEVEL_' == substr($name, 0, 6) && $value == $this->level) {
                $exists = true;
                break;
            }
        }

        if (! $exists) {
            throw new \InvalidArgumentException('Invalid user level specified');
        }

        $this->level = $level;
        return $this;
    }

    /**
     * @return \ReflectionClass
     */
    protected function getReflection()
    {
        if (null === self::$reflectionCache) {
            self::$reflectionCache = new \ReflectionClass(get_called_class());
        }

        return self::$reflectionCache;
    }

    /**
     * @return int
     */
    public function getLevel()
    {
        return $this->level;
    }
}