<?php

namespace User\Form;

use BnpBase\Form\ProvideEventsForm;
use BnpBase\Mapper\BaseMapperInterface;
use Zend\InputFilter\InputFilterProviderInterface;

class RegisterUser extends ProvideEventsForm implements
    InputFilterProviderInterface
{
    protected $userMapper;

    public function __construct(BaseMapperInterface $mapper, $name = null)
    {
        parent::__construct($name ?: 'Register');

        $this->userMapper = $mapper;

        $this->add(array(
            'name' => 'username',
            'type' => 'text',
            'options' => array(
                'label' => 'Username'
            )
        ));

        $this->add(array(
            'name' => 'password',
            'type' => 'password',
            'options' => array(
                'label' => 'Password'
            )
        ));

        $this->add(array(
            'name' => 'password_repeat',
            'type' => 'password',
            'options' => array(
                'label' => 'Repeat Password'
            )
        ));

        $this->add(array(
            'name' => 'submit',
            'type' => 'submit'
        ));
    }

    /**
     * Should return an array specification compatible with
     * {@link Zend\InputFilter\Factory::createInputFilter()}.
     *
     * @return array
     */
    public function getInputFilterSpecification()
    {
        return array(
            'username' => array(
                'required' => true,
                'filters' => array(
                    array('name' => 'StringTrim'),
                    array('name' => 'StripTags')
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 5,
                            'max' => 100
                        )
                    ),
                    array(
                        'name' => 'BnpBase\\Validator\\EntityNotExists',
                        'options' => array(
                            'mapper' => $this->userMapper,
                            'fields' => array('username')
                        )
                    )
                )
            ),
            'password' => array(
                'required' => true,
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 5,
                            'max' => 100
                        )
                    )
                )
            ),
            'password_repeat' => array(
                'required' => true,
                'validators' => array(
                    array(
                        'name' => 'Identical',
                        'options' => array(
                            'token' => 'password'
                        )
                    )
                )
            )
        );
    }
}