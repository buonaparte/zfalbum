<?php

namespace User\Form;

use BnpBase\Authentication\Adapter\MapperAdapter;
use BnpBase\Form\ProvideEventsForm;
use Zend\InputFilter\InputFilterProviderInterface;

class LoginUser extends ProvideEventsForm implements
    InputFilterProviderInterface
{
    /**
     * @var MapperAdapter
     */
    protected $authAdapter;

    public function __construct(MapperAdapter $authAdapter, $name = null)
    {
        parent::__construct($name ?: 'Login');

        $this->authAdapter = $authAdapter;

        $this->add(array(
            'name' => 'submit',
            'type' => 'submit'
        ));
    }

    public function init()
    {
        parent::init();

        $identityField = $this->authAdapter->getIdentityField();
        $credentialField = $this->authAdapter->getCredentialField();

        $this->add(array(
            'name' => $identityField,
            'type' => 'text',
            'attributes' => array(
                'placeholder' => $identityField
            )
        ));

        $this->add(array(
            'name' => $credentialField,
            'type' => 'password',
            'attributes' => array(
                'placeholder' => $credentialField
            )
        ));
    }

    /**
     * Should return an array specification compatible with
     * {@link Zend\InputFilter\Factory::createInputFilter()}.
     *
     * @return array
     */
    public function getInputFilterSpecification()
    {
        return array();
    }
}