<?php

namespace User\EventManager;

use Zend\Authentication\AuthenticationService;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\Http\Request;
use Zend\Http\Response;
use Zend\Mvc\Controller\ControllerManager;
use Zend\Mvc\Controller\Plugin\FlashMessenger;
use Zend\Mvc\Controller\Plugin\Redirect;
use Zend\Mvc\Controller\PluginManager;
use Zend\Mvc\MvcEvent;
use Zend\ServiceManager\ServiceLocatorInterface;
use ZfcRbac\Service\Rbac;

class DispatchErrorListenerAggregate implements ListenerAggregateInterface
{
    protected $listeners = array();

    /**
     * Attach one or more listeners
     *
     * Implementors may add an optional $priority argument; the EventManager
     * implementation will pass this to the aggregate.
     *
     * @param EventManagerInterface $events
     *
     * @return void
     */
    public function attach(EventManagerInterface $events)
    {
        $this->listeners[] = $events->attach(MvcEvent::EVENT_DISPATCH_ERROR, array($this, 'onDispatchError'), 1000);
    }

    /**
     * Detach all previously attached listeners
     *
     * @param EventManagerInterface $events
     *
     * @return void
     */
    public function detach(EventManagerInterface $events)
    {
        foreach ($this->listeners as $i => $listener) {
            if ($events->detach($listener)) {
                unset($this->listeners[$i]);
            }
        }
    }

    public function onDispatchError(MvcEvent $event)
    {
        $error = $event->getError();
        if (! $error || ! in_array($error, array(Rbac::ERROR_CONTROLLER_UNAUTHORIZED, Rbac::ERROR_ROUTE_UNAUTHORIZED))) {
            return null;
        }
//        die('here');

        /** @var $request Request */
        $request = $event->getApplication()->getRequest();

        /** @var $sm ServiceLocatorInterface */
        $sm = $event->getApplication()->getServiceManager();
        /** @var $auth AuthenticationService */
        $auth = $sm->get('Zend\Authentication\AuthenticationService');

        if (! $auth->hasIdentity()) {
            $url = $event->getRouter()->assemble(array(), array(
                'name' => 'user/login',
                'query' => array(
                    'back' => $request->getUriString()
                )
            ));
        } else {
            $url = $event->getRouter()->assemble(array(), array(
                'name' => 'home'
            ));

            /** @var $cm ControllerManager */
            $cm = $sm->get('ControllerPluginManager');
            /** @var $flashMessenger FlashMessenger */
            $flashMessenger = $cm->get('FlashMessenger');
            $flashMessenger->addErrorMessage('Not enough permissions');
        }

        /** @var $response Response */
        $response = $event->getApplication()->getResponse();
        $response->getHeaders()->addHeaderLine('location', $url);
        $response->setStatusCode(302);

        $event->stopPropagation();
    }
}