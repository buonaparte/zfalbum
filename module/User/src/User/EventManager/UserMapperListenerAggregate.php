<?php

namespace User\EventManager;

use BnpBase\Mapper\BaseMapper;
use User\Entity\UserInterface;
use User\Service\PasswordManager;
use Zend\Crypt\Password\Bcrypt;
use Zend\EventManager\EventInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;

class UserMapperListenerAggregate implements ListenerAggregateInterface
{
    /**
     * @var PasswordManager
     */
    protected $passwordManager;

    public function __construct(PasswordManager $service)
    {
        $this->passwordManager = $service;
    }

    /**
     * Attach one or more listeners
     *
     * Implementors may add an optional $priority argument; the EventManager
     * implementation will pass this to the aggregate.
     *
     * @param EventManagerInterface $events
     *
     * @return void
     */
    public function attach(EventManagerInterface $events)
    {
        $events->attach(array(BaseMapper::EVENT_CREATE, BaseMapper::EVENT_UPDATE), array($this, 'hashPassword'));
    }

    /**
     * Detach all previously attached listeners
     *
     * @param EventManagerInterface $events
     *
     * @return void
     */
    public function detach(EventManagerInterface $events)
    {
    }

    public function hashPassword(EventInterface $event)
    {
        /** @var $user UserInterface */
        $user = $event->getParam('entity');

        if (! $password = $user->getPassword()) {
            return;
        }

        $user->setPassword($this->passwordManager->createPasswordHash($password));
    }
}