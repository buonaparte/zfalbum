<?php

namespace User;

use BnpBase\Authentication\Adapter\MapperAdapter;
use BnpBase\Mapper\Adapter\DoctrineAdapter;
use BnpBase\Mapper\BaseMapper;
use BnpBase\Mapper\BaseMapperInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject;
use User\Controller\ManagementController;
use User\Controller\UserController;
use User\Entity\User;
use User\EventManager\UserMapperListenerAggregate;
use User\Form\LoginUser;
use User\Form\RegisterUser;
use User\Service\PasswordManager;
use Zend\Authentication\Adapter\AdapterInterface;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Storage\Session;
use Zend\Authentication\Storage\StorageInterface;
use Zend\Config\Config;
use Zend\Crypt\Password\Bcrypt;
use Zend\EventManager\EventInterface;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\EventManager\SharedEventManager;
use Zend\Form\FormElementManager;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\BootstrapListenerInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\ControllerProviderInterface;
use Zend\ModuleManager\Feature\FormElementProviderInterface;
use Zend\ModuleManager\Feature\ServiceProviderInterface;
use Zend\Mvc\Controller\ControllerManager;
use Zend\Mvc\MvcEvent;
use Zend\Navigation\Page\AbstractPage;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Session\Storage\SessionStorage;
use Zend\Stdlib\Hydrator\ClassMethods;
use Zend\Stdlib\Hydrator\HydratorInterface;
use ZfcRbac\Service\Rbac;

class Module implements
    ConfigProviderInterface,
    FormElementProviderInterface,
    ControllerProviderInterface,
    ServiceProviderInterface,
    AutoloaderProviderInterface,
    BootstrapListenerInterface
{
    /**
     * Returns configuration to merge with application configuration
     *
     * @return array|\Traversable
     */
    public function getConfig()
    {
        return include __DIR__.'/../../config/module.config.php';
    }

    /**
     * Expected to return \Zend\ServiceManager\Config object or array to seed
     * such an object.
     *
     * @return array|\Zend\ServiceManager\Config
     */
    public function getControllerConfig()
    {
        return array(
            'factories' => array(
                'User\Controller\User' => function (ControllerManager $cm) {
                    /** @var $sm ServiceLocatorInterface */
                    $sm = $cm->getServiceLocator();

                    /** @var $mapper BaseMapperInterface */
                    $mapper = $sm->get('User\Mapper\User');

                    /** @var $fm FormElementManager */
                    $fm = $sm->get('FormElementManager');
                    /** @var $registerForm RegisterUser */
                    $registerForm = $fm->get('User\Form\RegisterUser');

                    $controller = new UserController($mapper);
                    $controller->setRegisterForm($registerForm);

                    return $controller;
                },
                'User\Controller\Management' => function (ControllerManager $cm) {
                    /** @var $sm ServiceLocatorInterface */
                    $sm = $cm->getServiceLocator();

                    /** @var $mapper BaseMapperInterface */
                    $mapper = $sm->get('User\Mapper\User');

                    $controller = new ManagementController($mapper);

                    return $controller;
                }
            )
        );
    }

    /**
     * Expected to return \Zend\ServiceManager\Config object or array to
     * seed such an object.
     *
     * @return array|\Zend\ServiceManager\Config
     */
    public function getFormElementConfig()
    {
        return array(
            'factories' => array(
                'User\Form\RegisterUser' => function (FormElementManager $fm) {
                    /** @var $mapper BaseMapperInterface */
                    $mapper = $fm->getServiceLocator()->get('User\Mapper\User');
                    /** @var $hydrator HydratorInterface */
                    $hydrator = $fm->getServiceLocator()->get('User\Form\UserHydrator');

                    $form = new RegisterUser($mapper);
                    $form->setHydrator($hydrator);

                    return $form;
                },
                'User\Form\LoginUser' => function (FormElementManager $fm) {
                    /** @var $authAdapter MapperAdapter */
                    $authAdapter = $fm->getServiceLocator()->get('User\Service\Auth\Adapter');

                    return new LoginUser($authAdapter);
                }
            )
        );
    }

    /**
     * Expected to return \Zend\ServiceManager\Config object or array to
     * seed such an object.
     *
     * @return array|\Zend\ServiceManager\Config
     */
    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'User\Form\UserHydrator' => function (ServiceLocatorInterface $sm) {
                    /** @var $em ObjectManager */
                    $em = $sm->get('doctrine.entitymanager.orm_default');

                    return new DoctrineObject($em, 'User\Entity\User');
                },
                'User\Mapper\User' => function (ServiceLocatorInterface $sm) {
                    /** @var $em EntityManager */
                    $em = $sm->get('doctrine.entitymanager.orm_default');

                    /** @var $listener UserMapperListenerAggregate */
                    $listener = $sm->get('User\EventManager\UserMapperListenerAggregate');

                    $baseMapper = new BaseMapper(new DoctrineAdapter($em, 'User\Entity\User'));
                    $baseMapper->setEntityPrototype(new User());
                    $baseMapper->getEventManager()->attachAggregate($listener);

                    return $baseMapper;
                },
                'Bcrypt' => function (ServiceLocatorInterface $sm) {
                    $service = new Bcrypt();
                    $service->setCost(10);

                    return $service;
                },
                'User\Service\PasswordManager' => function (ServiceLocatorInterface $sm) {
                    /** @var $crypt Bcrypt */
                    $crypt = $sm->get('Bcrypt');

                    return new PasswordManager($crypt);
                },
                'User\EventManager\UserMapperListenerAggregate' => function (ServiceLocatorInterface $sm) {
                    /** @var $pm PasswordManager */
                    $pm = $sm->get('User\Service\PasswordManager');

                    return new UserMapperListenerAggregate($pm);
                },
                'User\Service\Auth\Storage' => function (ServiceLocatorInterface $sm) {
                    return new Session('zf_album');
                },
                'User\Service\Auth\Adapter' => function (ServiceLocatorInterface $sm) {
                    /** @var $mapper BaseMapper */
                    $mapper = $sm->get('User\Mapper\User');

                    $adapter = new MapperAdapter($mapper);
                    $adapter->setIdentityHydrator(new ClassMethods());
                    $adapter->setIdentityClass('User\Entity\UserInterface');
                    $adapter->setIdentityField('username');
                    $adapter->setCredentialField('password');

                    /** @var $pm PasswordManager */
                    $pm = $sm->get('User\Service\PasswordManager');
                    $adapter->setCredentialChecker(function ($plain, $hash) use ($pm) {
                        return $pm->checkPassword($plain, $hash);
                    });

                    return $adapter;
                },
                'AuthService' => function (ServiceLocatorInterface $sm) {
                    /** @var $adapter AdapterInterface */
                    $adapter = $sm->get('User\Service\Auth\Adapter');
                    /** @var $storage StorageInterface */
                    $storage = $sm->get('User\Service\Auth\Storage');

                    return new AuthenticationService(
                        $storage,
                        $adapter
                    );
                }
            ),
            'invokables' => array(
                'User\EventManager\DispatchErrorListenerAggregate' => 'User\EventManager\DispatchErrorListenerAggregate'
            ),
            'aliases' => array(
                'Zend\Authentication\AuthenticationService' => 'AuthService'
            )
        );
    }

    /**
     * Return an array for passing to Zend\Loader\AutoloaderFactory.
     *
     * @return array
     */
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__,
                    'ZfcRbac' => __DIR__.'/../../../../vendor/zf-commons/zfc-rbac'
                )
            )
        );
    }

    /**
     * Listen to the bootstrap event
     *
     * @param EventInterface $e
     * @return array
     */
    public function onBootstrap(EventInterface $e)
    {
        /** @var $e MvcEvent */
        /** @var $sharedEventManager SharedEventManager */
        $sharedEventManager = $e->getApplication()->getEventManager()->getSharedManager();
        $sharedEventManager->attach(
            'Zend\View\Helper\Navigation\AbstractHelper',
            'isAllowed',
            array($this, 'isAllowed')
        );

        /** @var $dispatchErrorListener ListenerAggregateInterface */
        $dispatchErrorListener = $e->getApplication()->getServiceManager()
            ->get('User\EventManager\DispatchErrorListenerAggregate');
        $e->getApplication()->getEventManager()->attachAggregate($dispatchErrorListener);
    }

    public function isAllowed(EventInterface $e)
    {
        $e->stopPropagation(true);

        /** @var $sm ServiceLocatorInterface */
        $sm = $e->getTarget()->getServiceLocator()->getServiceLocator();
        /** @var $rbac Rbac */
        $acl = $sm->get('ZfcRbac\Service\Rbac');
        /** @var $page AbstractPage */
        $page = $e->getParam('page');

        if (! $permission = $page->getPermission()) {
            return true;
        }

        return $acl->isGranted($permission);
    }
}