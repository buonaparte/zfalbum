<?php

namespace User\Controller;

use BnpBase\Authentication\Adapter\MapperAdapter;
use BnpBase\Mapper\BaseMapperInterface;
use User\Entity\User;
use User\Form\LoginUser;
use User\Form\RegisterUser;
use Zend\Authentication\AuthenticationService;
use Zend\Form\FormElementManager;
use Zend\Http\Request;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class UserController extends AbstractActionController
{
    /**
     * @var BaseMapperInterface
     */
    protected $userMapper;

    /**
     * @var RegisterUser
     */
    protected $registerForm;

    /**
     * @var LoginUser
     */
    protected $loginForm;

    /**
     * @var AuthenticationService
     */
    protected $authService;

    public function __construct(BaseMapperInterface $mapper)
    {
        $this->userMapper = $mapper;
    }

    public function setRegisterForm(RegisterUser $form)
    {
        $this->registerForm = $form;
    }

    public function setLoginForm(LoginUser $form)
    {
        $this->loginForm = $form;
    }

    public function getLoginForm()
    {
        if (null === $this->loginForm) {
            /** @var $fm FormElementManager */
            $fm = $this->getServiceLocator()->get('FormElementManager');
            /** @var $form LoginUser */
            $form = $fm->get('User\Form\LoginUser');
            $this->setLoginForm($form);
        }

        return $this->loginForm;
    }

    public function setAuthService(AuthenticationService $service)
    {
        $this->authService = $service;
        return $this;
    }

    public function getAuthService()
    {
        if (null === $this->authService) {
            /** @var $service AuthenticationService */
            $service = $this->getServiceLocator()->get('AuthService');
            $this->setAuthService($service);
        }

        return $this->authService;
    }

    public function getRegisterForm()
    {
        if (null === $this->registerForm) {
            /** @var $formManager FormElementManager */
            $formManager = $this->getServiceLocator()->get('FormElementManager');
            $this->registerForm = $formManager->get('User\Form\RegisterUser');
        }

        return $this->registerForm;
    }

    public function registerAction()
    {
        $form = $this->getRegisterForm();
        $form->bind($user = new User());

        /** @var $request Request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setData($request);
            if ($form->isValid()) {
                $this->userMapper->create($user);

                if ($user) {
                    $this->flashMessenger()->addSuccessMessage('Register Accepted');
                    $this->redirect()->toRoute('album');
                }
            }
        }

        return new ViewModel(array(
            'form' => $form
        ));
    }

    public function loginAction()
    {
        /** @var $request Request */
        $request = $this->getRequest();

        if ($this->getAuthService()->hasIdentity()) {
            if ($back = $request->getQuery('back')) {
                return $this->redirect()->toUrl($back);
            }

            return $this->redirect()->toRoute('album');
        }

        $model = array(
            'form' => $this->getLoginForm()
        );

        if ($back = $request->getQuery('back')) {
            $model['back'] = $back;
        }

        return new ViewModel($model);
    }

    public function authenticateAction()
    {
        $form = $this->getLoginForm();

        /** @var $request Request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setData($request);
            if ($form->isValid()) {
                $authService = $this->getAuthService();
                /** @var $adapter MapperAdapter */
                $adapter = $authService->getAdapter();
                $data = $form->getData();
                $adapter->setIdentity($data[$adapter->getIdentityField()])
                        ->setCredential($data[$adapter->getCredentialField()]);

                $result = $authService->authenticate();
                $messenger = $this->flashMessenger();

                if ($result->isValid()) {
                    foreach ($result->getMessages() as $message) {
                        $messenger->addInfoMessage($message);
                    }

                    if ($back = $request->getQuery('back')) {
                        return $this->redirect()->toUrl($back);
                    }

                    return $this->redirect()->toRoute('album');
                }

                foreach ($result->getMessages() as $message) {
                    $messenger->addErrorMessage($message);
                }
            }
        }

        $query = array();
        if ($back = $request->getQuery('back')) {
            $query['back'] = $back;
        }

        return $this->redirect()->toRoute('user/login', array(), array('query' => $query));
    }

    public function logoutAction()
    {
        $this->getAuthService()->clearIdentity();
        return $this->redirect()->toRoute('user/login');
    }
}