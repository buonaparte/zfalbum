<?php

namespace User\Controller;

use BnpBase\Mapper\BaseMapperInterface;
use BnpBase\Mapper\FilterInterface;
use User\Entity\UserInterface;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Paginator\Paginator;
use Zend\View\Model\ViewModel;

class ManagementController extends AbstractActionController
{
    /**
     * @var BaseMapperInterface
     */
    protected $userMapper;

    public function __construct(BaseMapperInterface $mapper)
    {
        $this->userMapper = $mapper;
    }

    public function indexAction()
    {
        $orderBy = $this->params()->fromRoute('order_by', 'id');
        $order = $this->params()->fromRoute('order', 'desc');

        /** @var $filter FilterInterface */
        $filter = $this->userMapper->getFilter();
        try {
            $filter->order($orderBy, $order);
        } catch (\Exception $e) {
            $filter->order('id');
        }
        $filter->add('parent', $this->identity()->getId());

        /** @var $users Paginator */
        $users = $this->userMapper->findAll($filter, true);

        $users->setCurrentPageNumber($page = $this->params()->fromRoute('page', 1));
        $users->setItemCountPerPage(10);

        return new ViewModel(array(
            'orderBy' => $orderBy,
            'order' => $order,
            'users' => $users,
            'page' => $page
        ));
    }

    public function addAction()
    {

    }

    protected function getUser($id)
    {
        $user = null;
        try {
            /** @var $user UserInterface */
            $user = $this->userMapper->findById($id);
            if (! $user || ! $user->getParent() || $this->identity()->getId() != $user->getParent()->getId()) {
                throw new \Exception();
            }
        } catch (\Exception $e) {
            return null;
        }

        return $user;
    }

    public function editAction()
    {
        if (null === $user = $this->getUser($id = $this->params()->fromRoute('id', 0))) {
            return $this->redirect()->toRoute('user/user');
        }
    }

    public function deleteAction()
    {
        if (null === $user = $this->getUser($id = $this->params()->fromRoute('id', 0))) {
            return $this->redirect()->toRoute('user/user');
        }
    }
}