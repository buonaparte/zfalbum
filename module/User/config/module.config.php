<?php

namespace User;

return array(
    'router' => array(
        'routes' => array(
            'user' => array(
                'type' => 'literal',
                'options' => array(
                    'route' => '/user',
                    'defaults' => array(
                        'controller' => 'User\Controller\User'
                    )
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'register' => array(
                        'type' => 'literal',
                        'options' => array(
                            'route' => '/register',
                            'defaults' => array(
                                'action' => 'register'
                            )
                        )
                    ),
                    'login' => array(
                        'type' => 'literal',
                        'options' => array(
                            'route' => '/login',
                            'defaults' => array(
                                'action' => 'login'
                            )
                        )
                    ),
                    'authenticate' => array(
                        'type' => 'literal',
                        'options' => array(
                            'route' => '/authenticate',
                            'defaults' => array(
                                'action' => 'authenticate'
                            )
                        )
                    ),
                    'logout' => array(
                        'type' => 'literal',
                        'options' => array(
                            'route' => '/logout',
                            'defaults' => array(
                                'action' => 'logout'
                            )
                        )
                    ),
                    'user' => array(
                        'type' => 'literal',
                        'options' => array(
                            'route' => '/management',
                            'defaults' => array(
                                'controller' => 'User\Controller\Management',
                                'action' => 'index'
                            ),
                        ),
                        'may_terminate' => true,
                        'child_routes' => array(
                            'list' => array(
                                'type' => 'segment',
                                'options' => array(
                                    'route' => '/page[/:page][/order/:order_by[-:order]]',
                                    'constraints' => array(
                                        'page' => '[1-9][0-9]*',
                                        'order_by' => '[a-zA-Z_]+',
                                        'order' => 'asc|desc'
                                    )
                                )
                            ),
                            'add' => array(
                                'type' => 'literal',
                                'options' => array(
                                    'route' => '/add',
                                    'defaults' => array(
                                        'action' => 'add'
                                    )
                                )
                            ),
                            'user' => array(
                                'type' => 'segment',
                                'options' => array(
                                    'route' => '[/:action][/:id]',
                                    'constraints' => array(
                                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                        'id' => '[0-9]+'
                                    ),
                                    'defaults' => array(
                                        'controller' => 'User\Controller\Management',
                                        'action' => 'index'
                                    )
                                )
                            )
                        )
                    )
                )
            )
        )
    ),
    'zfcrbac' => array(
        'identity_provider' => 'AuthService',
        'anonymousRole' => 'anonymous',
        'providers' => array(
            'ZfcRbac\Provider\Generic\Role\InMemory' => array(
                'roles' => array(
                    'anonymous',
                    'member',
                    'admin' => array('member')
                ),
            ),
            'ZfcRbac\Provider\Generic\Permission\InMemory' => array(
                'permissions' => array(
                    'anonymous' => array('anonymous'),
                    'member' => array('member'),
                    'admin' => array('admin', 'member')
                )
            )
        ),
        'firewalls' => array(
            'ZfcRbac\Firewall\Controller' => array(
                array(
                    'controller' => 'Album\Controller\Album',
                    'roles' => 'member'
                ),
                array(
                    'controller' => 'Album\Controller\Track',
                    'roles' => 'member'
                ),
                array(
                    'controller' => 'User\Controller\Management',
                    'roles' => 'admin'
                )
            )
        )
    ),
    'navigation' => array(
        'default' => array(
            'user' => array(
                'label' => 'User',
                'route' => 'user',
                'pages' => array(
                    'login' => array(
                        'route' => 'user/login',
                        'action' => 'login',
                        'label' => 'Login',
                        'permission' => 'anonymous'
                    ),
                    'register' => array(
                        'route' => 'user/register',
                        'action' => 'register',
                        'label' => 'Register',
                        'permission' => 'anonymous'
                    ),
                    'logout' => array(
                        'route' => 'user/logout',
                        'action' => 'logout',
                        'label' => 'Logout',
                        'permission' => 'member'
                    ),
                    'list' => array(
                        'route' => 'user/user/list',
                        'action' => 'index',
                        'label' => 'List',
                        'permission' => 'admin'
                    ),
                    'add' => array(
                        'route' => 'user/user/user',
                        'action' => 'add',
                        'label' => 'Add User',
                        'permission' => 'admin'
                    )
                )
            )
        )
    ),
    'view_manager' => array(
        'template_path_stack' => array(
           __DIR__.'/../view'
        )
    ),
    'doctrine' => array(
        'driver' => array(
            __NAMESPACE__ . '_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(
                    __DIR__ . '/../src/' . __NAMESPACE__ . '/Entity'
                )
            ),
            'orm_default' => array(
                'drivers' => array(
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                )
            )
        )
    )
);